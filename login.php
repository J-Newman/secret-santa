<?php
  session_start();
  $message="";
  if(isset($_POST['login'])){
    if(!isset( $_POST['email'], $_POST['password']))
    {
        $message = 'Please enter a valid username and password';
    }else{
      $conn = new mysqli("localhost","root","","secretsanta");

      if ($conn->connect_errno)
      {
          die( "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error );
      }

      $email = filter_var(strtolower(trim($_POST['email'], FILTER_SANITIZE_STRING)));
      $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
      /* Validate Login */
      $query = "SELECT id FROM people WHERE emailaddress = ? AND passcode = ? LIMIT 1";
      $userid = 0;
      if( $stmt = $conn->prepare($query) )
      {
        $stmt->bind_param('ss', $email, $password );
        $stmt->execute();
        $stmt->bind_result($id);

        if($stmt->fetch())
        {
          $stmt->close();
          $userid = $id;
        }
        else
        {
          $userid = 0;
        }

      }

      if($userid==0){
        $valid = false;
        $message = 'Wrong details entered.';
      }else{
        $valid = true;
      }

      if($valid){
        $_SESSION['user_id'] = $userid;
        header("location:index.php");
      }

    }
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login - Secret Santa - Focus</title>

    <!-- Bootstrap core CSS -->
    <link href="/bs/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/styles.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Mountains+of+Christmas:400,700|Pacifico' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin" action="" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php
          if($message != ""){
            ?>
            <div class="alert alert-danger">
              <?php echo $message; ?>
            </div>
            <?php
          }
        ?>
        <input name="email" type="text" class="form-control" placeholder="Email address" required autofocus>
        <input name="password" type="password" class="form-control" placeholder="Password" required>
        <input type="hidden" name="login">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
