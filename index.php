<?php
$interestsstring="";
	if(!isset($_SESSION)){
  		session_start();
	}

	if(!isset($_SESSION['user_id'])){
        header("location:login.php");
	}
	$name="";
	$conn = new mysqli("localhost","root","","secretsanta");

	if ($conn->connect_errno)
	{
	    die( "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error );
	}
	$query = 'SELECT giftee FROM gifts WHERE gifter = ?';
	//$sql = 'SELECT casino_id FROM user_to_casinos WHERE user_id = "2"';
	$gifteeid = 0;
	if( $stmt = $conn->prepare($query) )
	{
		$stmt->bind_param('i', $_SESSION['user_id']);
		$stmt->execute();
		$stmt->bind_result($giftee);

		if($stmt->fetch())
		{
			$stmt->close();
			$gifteeid = $giftee;
		}
		else
		{
			$gifteeid = 0;
		}
	}
	if($gifteeid==0){
		$name = 'No Name Found!';
	}else{
		$query = 'SELECT name,interests FROM people WHERE id = ?';
		if( $stmt = $conn->prepare($query) )
		{
			$stmt->bind_param('i', $gifteeid);
			$stmt->execute();
			$stmt->bind_result($gifteename,$interests);

			if($stmt->fetch())
			{
				$stmt->close();
				$name = $gifteename;
				$interests = $interests;
				$interestsstring="";
				if($interests != ""){
					$interestsstring = "<p>Dear Secret,<br/>";
					$interestsstring .= "This year I would like things related to:</p>";
					$interestsstring .= $interests;
					$interestsstring .= "<p>Thank You!<br/><br/></p>";
				}
			}
			else
			{
				$name = 'No Name Found!';
				$interestsstring ="";
			}
		}
	}



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Secret Santa - Focus</title>

    <!-- Bootstrap core CSS -->
    <link href="/bs/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/content.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Mountains+of+Christmas:400,700|Pacifico' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li class="active"><a href="/logout.php">Logout</a></li>
        </ul>
        <h3 class="text-muted">&nbsp;</h3>
      </div>

      <div class="jumbotron">
        <h1>Your Secret Santa is:</h1>
        <h2><?php echo $name; ?><br/><?php echo $interestsstring; ?></h2>
        <h3>Hover over the block above to see your secret santa</h3>
      </div>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
