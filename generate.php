<?php
  die();
  $conn = new mysqli("localhost","root","","secretsanta");

  if ($conn->connect_errno)
  {
      die( "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error );
  }
  $sql = 'SELECT * FROM people';
  $result = $conn->query($sql);

  $emails = array();
  if($result->num_rows > 0) {
      while($row = mysqli_fetch_assoc($result)) {
      	$emails[] = $row['id'];
      }
  }
  $matches = array();

  // create random keys for givers and givees
  $givers = range(1,count($emails));
  $givees = range(1,count($emails));

  do {
       // mix up the order of the ids
       shuffle($givers);
       shuffle($givees);
       // merge to one array
       $matches = array_combine($givers, $givees);
       // loop array checking if anybody has drawn themselves
       foreach ($matches as $er => $ee) {
            // if someone is drawn themselves start again and reshuffle
            if ($er == $ee){
            	continue 2;
            }
       }

       break;

  } while (true);


  // clear the database
  $sql = 'TRUNCATE TABLE gifts';
  $conn->query($sql);

  // populate the database
  foreach ($matches as $er => $ee) {
      $sql = 'INSERT into gifts (gifter,giftee) VALUES ('.$er.','.$ee.')';
  	$conn->query($sql);
  }

?>