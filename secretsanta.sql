/*
Navicat MySQL Data Transfer

Source Server         : local Wamp
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : secretsanta

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2013-11-07 09:24:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gifts`
-- ----------------------------
DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gifter` int(11) NOT NULL,
  `giftee` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gifter` (`gifter`),
  KEY `giftee` (`giftee`),
  CONSTRAINT `gifts_ibfk_1` FOREIGN KEY (`gifter`) REFERENCES `people` (`id`),
  CONSTRAINT `gifts_ibfk_2` FOREIGN KEY (`giftee`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gifts
-- ----------------------------

-- ----------------------------
-- Table structure for `people`
-- ----------------------------
DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `emailaddress` varchar(1000) NOT NULL,
  `passcode` varchar(255) NOT NULL,
  `interests` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of people
-- ----------------------------
INSERT INTO `people` VALUES ('1', 'Mike', 'michael@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('2', 'Phil', 'phil@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('3', 'Ian', 'ian@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('4', 'Hayden', 'hayden@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('5', 'Jamie', 'jamie@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('6', 'Ross', 'ross@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('7', 'Abbie', 'abigail@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('8', 'Sam', 'samuelroberts@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('9', 'Ben', 'ben.thurgood@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('10', 'Amy', 'amyw@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('11', 'Georgina', 'georgina@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('12', 'Sami', 'sami@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('13', 'Clairre', 'clarrie@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('14', 'Dan Betts (Betty)', 'dan@focusintegrated.co.uk', '', '');
INSERT INTO `people` VALUES ('15', 'Dan D (Double D)', 'daniel.drury@focusintegrated.co.uk', '', '');
